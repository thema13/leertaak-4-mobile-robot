package model.robot;

import model.virtualmap.OccupancyMap;

import java.io.PipedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.PipedOutputStream;
import java.io.IOException;

import java.util.StringTokenizer;

/**
 * Title    :   The Mobile Robot Explorer Simulation Environment v2.0
 * Copyright:   GNU General Public License as published by the Free Software Foundation
 * Company  :   Hanze University of Applied Sciences
 *
 * @author Dustin Meijer        (2012)
 * @author Alexander Jeurissen  (2012)
 * @author Davide Brugali       (2002)
 * @version 2.0
 */

public class MobileRobotAI implements Runnable {

	private final OccupancyMap map;
	private final MobileRobot robot;

    //AI constants
    private static final double ROBOT_SIZE = 25;

	private boolean running;

    //state variables
    private String result;
    private double position[] = new double[3];
    private double measures[] = new double[360];
    private PipedInputStream pipeIn;
    private BufferedReader input;
    private PrintWriter output;
    private int[] startingPosition = new int[2];
    private boolean followingAWall = false;
    private boolean startPosIsSet = false;
    private int stepsAfterWall = 0;

    public MobileRobotAI(MobileRobot robot, OccupancyMap map) {
		this.map = map;
		this.robot = robot;

	}

	/**
	 * In double[]this method the gui.controller sends commands to the robot and its devices.
	 * At the moment all the commands are hardcoded.
	 * The exercise is to let the gui.controller make intelligent decisions based on
	 * what has been discovered so far. This information is contained in the OccupancyMap.
	 */
	public void run() {
		this.running = true;
		while (running) {
			try {

				this.pipeIn = new PipedInputStream();
				this.input = new BufferedReader(new InputStreamReader(pipeIn));
				this.output = new PrintWriter(new PipedOutputStream(pipeIn), true);

				robot.setOutput(output);

//      ases where a variable value is never used after its assignment, i.e.:
				System.out.println("intelligence running");


                wallFollow();
                while(running){
                    System.out.println("༼ つ ◕_◕ ༽つdodododonee!!༼ つ ◕_◕ ༽つ");
                }

			} catch (IOException ioe) {
				System.err.println("execution stopped");
				running = false;
			}
		}

	}

    private void wallFollow(){
        followingAWall = false;
        startPosIsSet = false;
        stepsAfterWall = 0;

        try {
            robot.sendCommand("R1.GETPOS");
            result = input.readLine();
            parsePosition(result);

            int lastScanTimer = 0;

            while(!doneExploring()) {
                if (scanIsRequired() && lastScanTimer <= 0){
                    robot.sendCommand("L1.SCAN");
                    robot.sendCommand("S1.SCAN");

                    result = input.readLine();
                    parseMeasures(result);
                    map.drawLaserScan(position, measures);

                    result = input.readLine();
                    parseMeasures(result);
                    map.drawLaserScan(position, measures);

                    lastScanTimer = 2;
                }
                else if (seesCorridor() && followingAWall) {
                    robot.sendCommand("P1.ROTATERIGHT " + 90.0);
                    result = input.readLine();

                    robot.sendCommand("P1.MOVEFW " + 1);
                    result = input.readLine();
                }
                else if (isObstructed()) {
                    robot.sendCommand("P1.ROTATELEFT " + 90.0);
                    result = input.readLine();

                    followingAWall = true;
                }
                else{ //nothing else has happend (WE ARE FREE TO DRIVE!!)
                    robot.sendCommand("P1.MOVEFW " + map.CELL_DIMENSION);
                    result = input.readLine();
                }

                robot.sendCommand("R1.GETPOS");
                result = input.readLine();
                parsePosition(result);

                lastScanTimer -= 1;
            }

        }catch (IOException ioe) {
            System.err.println("execution stopped: I/O Exeption was trown in pipeline communication");
            running = false;
        }
    }

    public boolean Between(int a, int b, int c){
        if((a >= b && a <= c) || (a <= b && a >= c)){
            return true;
        }
        return false;
    }

    public boolean Between(double a, double b, double c){
        if((a >= b && a <= c) || (a <= b && a >= c)){
            return true;
        }
        return false;
    }
    private boolean NothingSpecialHappend() {
        return true;
    }

    //dangeroes !!!
    private boolean isObstructed() {
        int robotRow = (int)Math.round(position[0]/map.CELL_DIMENSION);
        int robotColumn = (int)Math.round(position[1]/map.CELL_DIMENSION);
        int startingRowIndex;
        int endingRowIndex;
        int startingColumnIndex;
        int endingColumnIndex;

        if (Between(position[2], 315, 360) || Between(position[2], 0, 45)){
            startingRowIndex = robotRow+1;
            endingRowIndex = robotRow+3;
            startingColumnIndex = robotColumn-2;
            endingColumnIndex = robotColumn+2;
        }
        else if (Between(position[2], 45, 135)){
            startingRowIndex = robotRow-2;
            endingRowIndex = robotRow+2;
            startingColumnIndex = robotColumn+1;
            endingColumnIndex = robotColumn+3;
        }
        else if (Between(position[2], 135, 225)){
            startingRowIndex = robotRow-3;
            endingRowIndex = robotRow-1;
            startingColumnIndex = robotColumn-2;
            endingColumnIndex = robotColumn+2;
        }
        else {
            startingRowIndex = robotRow-2;
            endingRowIndex = robotRow+2;
            startingColumnIndex = robotColumn-3;
            endingColumnIndex = robotColumn-1;
        }

        for(int i = startingRowIndex; i <= endingRowIndex; i++){
            for (int j = startingColumnIndex; j <= endingColumnIndex; j++){
                if(map.grid[i][j] == map.OBSTACLE) return true;
            }
        }
        return false;
    }

    private boolean seesCorridor() {
        int robotRow = (int)Math.round(position[0]/map.CELL_DIMENSION);
        int robotColumn = (int)Math.round(position[1]/map.CELL_DIMENSION);
        int startingRowIndex;
        int endingRowIndex;
        int startingColumnIndex;
        int endingColumnIndex;

        if (Between(position[2], 315, 360) || Between(position[2], 0, 45)){
            startingRowIndex = robotRow-2;
            endingRowIndex = robotRow+3;
            startingColumnIndex = robotColumn+1;
            endingColumnIndex = robotColumn+3;
        }
        else if (Between(position[2], 45, 135)){
            startingRowIndex = robotRow-3;
            endingRowIndex = robotRow-1;
            startingColumnIndex = robotColumn-2;
            endingColumnIndex = robotColumn+3;
        }
        else if (Between(position[2], 135, 225)){
            startingRowIndex = robotRow-3;
            endingRowIndex = robotRow+2;
            startingColumnIndex = robotColumn-3;
            endingColumnIndex = robotColumn-1;
        }
        else {
            startingRowIndex = robotRow+1;
            endingRowIndex = robotRow+3;
            startingColumnIndex = robotColumn-3;
            endingColumnIndex = robotColumn+2;
        }

        for(int i = startingRowIndex; i <= endingRowIndex; i++){
            for (int j = startingColumnIndex; j <= endingColumnIndex; j++){
                if(map.grid[i][j] == map.OBSTACLE) return false;
            }
        }
        return true;
    }

    private boolean scanIsRequired() {
        int robotRow = (int)Math.round(position[0]/map.CELL_DIMENSION);
        int robotColumn = (int)Math.round(position[1]/map.CELL_DIMENSION);
        int startingRowIndex;
        int endingRowIndex;
        int startingColumnIndex;
        int endingColumnIndex;

        if (Between(position[2], 315, 360) || Between(position[2], 0, 45)){
            startingRowIndex = robotRow-2;
            endingRowIndex = robotRow+2;
            startingColumnIndex = robotColumn-1;
            endingColumnIndex = robotColumn+3;
        }
        else if (Between(position[2], 45, 135)){
            startingRowIndex = robotRow-3;
            endingRowIndex = robotRow+1;
            startingColumnIndex = robotColumn-2;
            endingColumnIndex = robotColumn+2;
        }
        else if (Between(position[2], 135, 225)){
            startingRowIndex = robotRow-2;
            endingRowIndex = robotRow+2;
            startingColumnIndex = robotColumn-3;
            endingColumnIndex = robotColumn+1;
        }
        else {
            startingRowIndex = robotRow-1;
            endingRowIndex = robotRow+3;
            startingColumnIndex = robotColumn-2;
            endingColumnIndex = robotColumn+2;
        }

        for(int i = startingRowIndex; i <= endingRowIndex; i++){
            for (int j = startingColumnIndex; j <= endingColumnIndex; j++){
                if(map.grid[i][j] == map.UNKNOWN) return true;
            }
        }
        return false;
    }

    private boolean doneExploring() {
        if (startPosIsSet){
            ++stepsAfterWall;
        }
        if(followingAWall && !startPosIsSet){
            startingPosition[0] = (int)Math.round(position[0]/map.CELL_DIMENSION);
            startingPosition[1] = (int)Math.round(position[2]/map.CELL_DIMENSION);
            startPosIsSet = true;
        }
        if(Between((int)Math.round(position[0]/map.CELL_DIMENSION), startingPosition[0] - 1,  startingPosition[0] + 1)
                && Between((int)Math.round(position[1]/map.CELL_DIMENSION), startingPosition[1] - 1,  startingPosition[1] + 1)
                && stepsAfterWall > 15) {
            return true;
        }

        return false;
    }


    //takes a vector and returns the length in the original unit
    public double getVectorLength(double[] vector) {
        return Math.sqrt(Math.pow(vector[0], 2) + Math.pow(vector[1], 2));
    }

    //takes a vector and returns the direction in degrees compared to x-axis
    public double getVectorDirection(double[] vector) {
        return Math.toDegrees(Math.atan2(vector[1], vector[0]));
    }

    private void parsePosition(String value) {
		int indexInit;
		int indexEnd;
		String parameter;

		indexInit = value.indexOf("X=");
		parameter = value.substring(indexInit + 2);
		indexEnd = parameter.indexOf(' ');
		position[0] = Double.parseDouble(parameter.substring(0, indexEnd));

		indexInit = value.indexOf("Y=");
		parameter = value.substring(indexInit + 2);
		indexEnd = parameter.indexOf(' ');
		position[1] = Double.parseDouble(parameter.substring(0, indexEnd));

		indexInit = value.indexOf("DIR=");
		parameter = value.substring(indexInit + 4);
		position[2] = Double.parseDouble(parameter);
	}

	private void parseMeasures(String value) {
		for (int i = 0; i < 360; i++) {
			measures[i] = 100.0;
		}
		if (value.length() >= 5) {
			value = value.substring(5);  // removes the "SCAN " keyword

			StringTokenizer tokenizer = new StringTokenizer(value, " ");

			double distance;
			int direction;
			while (tokenizer.hasMoreTokens()) {
				distance = Double.parseDouble(tokenizer.nextToken().substring(2));
				direction = (int) Math.round(Math.toDegrees(Double.parseDouble(tokenizer.nextToken().substring(2))));
				if (direction == 360) {
					direction = 0;
				}
				measures[direction] = distance;
				// Printing out all the degrees and what it encountered.
				//if(direction < 100.0)System.out.println("direction = " + direction + " distance = " + distance);
			}
		}
	}


}
